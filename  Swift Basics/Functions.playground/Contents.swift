import UIKit


func myFunction(){
    print ("Hello World")
    
}

myFunction() // => Hello World

func sumFunction(x: Int, y: Int){
    var result = x + y //result = 6
    print (result)
}

sumFunction(x: 2, y: 4) // =>6
sumFunction(x: 50, y: 60) // => 110
