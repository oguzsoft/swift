import UIKit

///// ------------ VARİABLES ------------ //////
///// --- String Values ---- /////

var name = "Oguzkaan" // var variable type is changeable

var userName = "Gündüz"

let pi_number = 3.14 // Define a pi number. pi_number is constant variable. unchangeable!

userName = "Gece"  // userName is changed.

/// String values methods

userName.append("eeee") // Added 'eeee' to userName variable => "Geceeeee"

userName.uppercased() // uppercase => "GECEEEEE"

userName.lowercased() // lowercase => "geceeeee"

///// --- Integer Values ---- /////

// integer & double & float

let pi = 3.14 // float&double

var age = 30 //integer

age * 4 //=> 120

print("Age class = ", type(of: age)) //Print age's class name => Int

print("Pi class = ",  type(of: pi))  //Print pi's class name => Double

Double(age) // Change age type

Double(age) + pi // I can sum age and pi => 33.14

var myNumber : Int = 35

var myString : String  = "Hello World"


