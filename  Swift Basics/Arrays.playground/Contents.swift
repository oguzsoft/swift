import UIKit
///// ------ ARRAYS -----
var favoriteMovie = ["Recep İvedik", "Cebimdeki Yabanci", "Dag", "Ezel", "Titanic"]
favoriteMovie //View arrays elements
favoriteMovie[0] //view 0. element
favoriteMovie[1] //=>Cebimdeki Yabanci
favoriteMovie[1] = "3 Idiots"
favoriteMovie[1] //=>3 Idiots

favoriteMovie.count //=>5 //Total number of elements

favoriteMovie.append("Nefes") //Add an element to last index of array

favoriteMovie.isEmpty //=> false

favoriteMovie.sort()
