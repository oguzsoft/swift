import UIKit
///// -------- CLASSES --------
class Car{
    var brand = ""
    var modelYear = 0
    var price = 0
}

var car1 = Car() //Create new car
car1.brand = "Mercedes"
car1.modelYear = 2012
car1.price = 200000

print (car1.brand) //=>Mercedes
