import UIKit
///// --------- DİCTİONARİES -----------
var cityPlates = ["İstanbul" : "34",    //Create dictionaries
                  "Kırklareli" : "39",
                  "Tekirdağ" : "59"]
cityPlates["İstanbul"] //=>34
cityPlates["Tekirdağ"] = "00" //You can change key's value

cityPlates["Tekirdağ"] //=> 00

cityPlates["00"] //=>nil Because "00" is value. Can't write value. Only keys..

