import UIKit
///// -------- If - Else --------

var myAge = 20

if myAge < 24{
    print ("Young Category")
}
else{
    print ("Adult Category")
}
//=> Young Category

//----------------------------------------

if myAge<6{
    print("Free Category")
}
else if myAge >= 6 && myAge <= 24{
    print("Young Caterory")
}
else{
    print("Adult Category")
}
//=> Young Category
