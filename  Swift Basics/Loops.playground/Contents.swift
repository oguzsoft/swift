import UIKit
// ------ LOOPS ------
var sayac = 1
//While loop
while sayac<5{          //sayac variable up to 5
    puts ("Hello World")//4 times write "Hello World"
    sayac+=1
}

//For loop

var myFavoriteCars = ["Mercedes", "Bmw", "Volkswagen", "Jeep"]

for car in myFavoriteCars{ //Write each elemets of myFavoriteCars
    puts(car)
}

var myNumberArray = [1,2,3,4,5]
for number in myNumberArray{ //multiply array elements by 5
    print (number * 5)
}

var max = 10
for number in max ... 20{ //Write from 10 to 20
    print (number)
}
